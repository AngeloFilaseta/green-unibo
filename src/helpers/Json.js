function cats(){
    return {
        "Comunicazione Digitale": "I documenti cartacei rilevanti dal punto di vista giuridico e amministrativo sono " +
            "ripensati realizzati in formato digitale, garantendo comunque i requisiti tecnici e legali necessari.",

        "Dematerializzazione": "I documenti cartacei rilevanti dal punto di vista giuridico e amministrativo sono " +
            "ripensati realizzati in formato digitale, garantendo comunque i requisiti tecnici e legali necessari.",

        "Innovazione del Processo": "I documenti cartacei rilevanti dal punto di vista giuridico e amministrativo sono " +
            "ripensati realizzati in formato digitale, garantendo comunque i requisiti tecnici e legali necessari.",
    };
}

function args(){
    return {
        "Almaorienta": "Periodicamente l’Università di Bologna raccoglie in forma anonima le opinioni degli studenti " +
            "e delle studentesse sulle attività didattiche.\n" +
            "Dal 2016 il questionario online ha sostituito quello cartaceo.",

        "Atti Organi Ateneo" : "",

        "Contratti di Docenza" : "La gestione degli incarichi di insegnamento coinvolge ogni anno circa 4.000 persone." +
            "Dal 2017 il nuovo applicativo permette al personale docente dell’Università di bologna di gestire online " +
            "tutta la fase di stipula dei contratti.",

        "Contratti per RTD" : "",

        "Passaggio Nuova Firma Digitale": "Nel 2018 il passaggio dal vecchio sistema di firma remota alla nuova firma " +
            "digitale è stato gestito da un applicativo che ha rimosso completamente la necessità di stampare i "+
            "contratti per richiedere la nuova firma digitale.",

        "Questionario Opinione Studenti" : "Periodicamente l’Università di Bologna raccoglie in forma anonima le " +
            "opinioni degli studenti e delle studentesse sulle attività didattiche.\n" +
            "Dal 2016 il questionario online ha sostituito quello cartaceo.",

        "Tesi Digitali": "I laureandi e le laureande dell'Università di Bologna consegnano le tesi di laurea solo " +
            "in formato digitale. \n AMS Tesi di Laurea è il servizio che raccoglie e conserva le " +
            "tesi di laurea in formato digitale",

        "Unatantum":""
    };
}

function dets(){
    return {
        "Comunicazione Digitale": ["Almaorienta"],

        "Dematerializzazione": ["Tesi Digitali", "Contratti Per RTD", "Atti Organi Ateneo", "Contratti di Docenza",
            "Questionario Opinione Studenti"],

        "Innovazione del Processo": ["Passaggio Nuova Firma Digitale", "Unatantum"]
    };
}

export function categoryDescription(id){
    return cats()[id];
}

export function detailsItems(id){
    return dets()[id];
}

export function argumentsDescription(id){
    return args()[id];
}

export function description(id){
    return cats().hasOwnProperty(id) ? categoryDescription(id) :
           args().hasOwnProperty(id) ? argumentsDescription(id) : "No description found!";
}




