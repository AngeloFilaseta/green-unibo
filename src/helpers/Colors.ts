export enum Colors {
    White = "#FFFFFF",
    Black = "#000000",
    Blue = "#2D4374",
    Yellow = "#F9CE62",
    Cyan = "#B2E7ED",
    Green = "#2ea180"
}