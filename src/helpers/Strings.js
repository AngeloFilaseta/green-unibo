import voca from "voca";

/**
 * Return the number in a dot notation, ex: (1234567 -> 1.234.567).
 * @param num the number.
 * @returns the number in dot notation.
 */
export function dotNotationNumber(num) {
        num += '';
        let x = num.split('.');
        let x1 = x[0];
        let x2 = x.length > 1 ? '.' + x[1] : '';
        let rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1.$2');
        }
        return x1 + x2;
}

/**
 * Insert a newline (\n) char after a min of 10 chars in the next possible space char.
 * @param str the original string
 * @returns {string|void|*} the string with newlines
 */
export function newLineStr(str) {
    return str.replace(/(.{6}[^ ]* )/g, "$1\n");
}

/**
 * @param campus the string of the props, for example "cesena"
 * @returns {string} the entire name Capitalized, for example "Cesena Campus"
 */

export function campusFullName(campus){
    if(campus != null){
        return voca.capitalize(campus) + " Campus";
    }
    return "";
}

/**
 * @param campus the string of the props, for example "cesena"
 * @returns {string} the entire name Capitalized, for example "Cesena \n Campus"
 */

export function campusFullNameNewLine(campus) {
    return campusFullName(campus).split(" ").join("\n");
}