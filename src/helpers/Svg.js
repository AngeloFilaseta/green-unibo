const DELTA_RADIUS_TITLE = 0.55;
const DELTA_LENGTH_TITLE = -0.7;

const DELTA_RADIUS_TEXT = 4;
const DELTA_LENGTH_TEXT = 5;

const DELTA_SPACE_RADIUS = 3;


/**
 * Return the font size in px that should be used in the title inside an svg circle using its radius.
 * @param radius the radius of the circle
 * @param titleLength the length of the title
 * @returns {string} the font size in px that should be used is the circle
 */
export function titleFontSizeFromRadius(radius, titleLength){
     /*Math.round(radius / D_TITLE * ) + "px";*/
    let b = 2*Math.sqrt(radius * radius);
    return Math.min(radius * DELTA_RADIUS_TITLE,
                     b/(titleLength + DELTA_LENGTH_TITLE)) + "px";
}

/**
 *  Return the font size in px that should be used in the text inside an svg circle using its radius.
 * @param radius the radius of the circle
 * @param textLength the the length of the text
 * @returns {string} the font size in px that should be used is the circle
 */
export function textFontSizeFromRadius(radius, textLength){
    let b = 2*Math.sqrt(radius * radius);
    return Math.min(radius / DELTA_RADIUS_TEXT,
                    b / (textLength - DELTA_LENGTH_TEXT)) + "px";
}

/**
 *  Return the space in px between title and text label in svg circles.
 * @param radius the radius of the circle
 * @returns {number} the space in px between title and text
 */
export function deltaSpaceFromRadius(radius){
    return radius / DELTA_SPACE_RADIUS;
}