/**
 *
 * @returns {string[]} the list of all the campus used in some ProgressBar
 */
export function getCampusList(){
     return ["Cesena", "Forlì", "Navile", "Ravenna", "Rimini"];
}

/**
 *
 * @returns {string[]} the list of all the Years used in some ProgressBar
 */
export function getYearsList(){
    return ["2016", "-", "2017","-", "2018", "-", "2019"];
}

/**
 *
 * @param array the array to iterate
 * @returns {{next: (function(): *)}}  by calling next, value and done are modified
 */
export function makeIterator(array) {
    let nextIndex = 0;
    return {
        next: function() {
            return nextIndex < array.length ?
                {value: array[nextIndex++], done: false} :
                {value: "", done: true};
        }
    };
}

/**
 * @param min the ceil number
 * @param max the floor number
 * @returns {number} a random number between min e max
 * The maximum is exclusive and the minimum is inclusive
 */
export function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

/**
 *
 * @param startValue the start value
 * @param stopValue the value of finish
 * @param cardinality how many values in the array
 * @returns [] array containing cardinality values, equally spaced and as integer, from startValue to stopValue
 */
export function linspace(startValue, stopValue, cardinality) {
    const arr = [];
    const step = (stopValue - startValue) / (cardinality - 1);
    for (let i = 0; i < cardinality; i++) {
        arr.push(parseInt(startValue + (step * i)));
    }
    return arr;
}