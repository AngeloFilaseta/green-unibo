import React from "react";
import Category from "../components/Category";
import ProgressWithItems from "../components/ProgressWithItems";
import {getYearsList} from "../helpers/Helpers";
import ClickMeLabel from "../components/ClickMeLabel";
import ReactAnime from "react-animejs";
const {Anime} = ReactAnime;

class CategoriesContent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isLarge: false,
            treeSelected: -1
        };
        this.updatePredicate = this.updatePredicate.bind(this);
        this.treeClick = this.treeClick.bind(this);
    }

    componentDidMount() {
        this.updatePredicate();
        window.addEventListener("resize", this.updatePredicate);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updatePredicate);
    }

    updatePredicate() {
        //Change on Large Screen (lg in bootstrap)
        this.setState({ isLarge: window.innerWidth >= 992 });
    }

    treeClick(treeClicked){
        this.setState({ treeSelected: treeClicked });
    }

    render(){

        let isDesktop = this.state.isLarge;
        let treeSelected = this.state.treeSelected;
        let progressValues = getYearsList();
        let selectedValue = "2016";

        let categories = ["Comunicazione Digitale", "Dematerializzazione","Innovazione del Processo"];
        let categories_icons = ["sms", "book", "favorite"];

        return(
            <>
                <div className="cat row h-100  pb-5 pt-6 align-items-end">
                    {isDesktop &&
                    <div className="progress_div position-absolute pt-0">
                        <ProgressWithItems list={progressValues} selected={selectedValue} vertical={true} textBot={true}/>
                    </div>
                    }
                    <div className="col-12 col-lg-2 pt-6 mb-5 mb-md-0">
                        {!isDesktop &&
                        <ProgressWithItems list={progressValues} selected={selectedValue}/>
                        }
                    </div>

                    <div className="tree_front col-lg-3" onClick={() => this.treeClick(0)}>
                        <Category catId={1} name={categories[0]} treeNumber={8}
                                  treeSpacing="px-6" icon={categories_icons[0]} selected = {treeSelected === 0}/>
                        <div className={"position-absolute click"}>
                            {(treeSelected === -1 && isDesktop === true) &&
                                <Anime initial={[{
                                    targets: '.click_me',
                                    keyframes: [
                                        {
                                            opacity:[0,1],
                                            easing: 'linear',
                                        },
                                        {
                                            duration: 2000,
                                            translateX: "-11vw",
                                            translateY: "6vh",
                                            easing: 'linear',
                                            endDelay:3000
                                        },
                                        {
                                            duration: 1000,
                                            opacity:[1,0],
                                            easing: 'linear',
                                            endDelay: 10000
                                        }
                                    ],
                                    loop: true
                                }]}>
                                    <ClickMeLabel/>
                                </Anime>
                            }
                        </div>
                    </div>
                    <div className="tree_front col-lg-3" onClick={() => this.treeClick(1)}>
                        <Category catId={2} name={categories[1]} treeNumber={9}
                                  treeSpacing="px-5 mx-3" icon={categories_icons[1]} selected = {treeSelected === 1}/>
                    </div>
                    <div className="tree_front col-lg-3" onClick={() => this.treeClick(2)}>
                        <Category catId={3} name={categories[2]} treeNumber={6}
                                  treeSpacing="px-6" icon={categories_icons[2]} selected = {treeSelected === 2}/>
                    </div>
                </div>
             </>
        );
    }

}

export default CategoriesContent;