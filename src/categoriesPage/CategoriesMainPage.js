import React from 'react';

import '../components/BackArrow'
import './CategoriesMainPage.css';
import CategoriesContent from "./CategoriesContent";
import TopBar from "../components/TopBar";

class CategoriesMainPage extends React.Component {

    render() {

        let backText = "Back";

        return (
            <>
                <TopBar campus={this.props.campus} backText={backText} backPath={"/" + this.props.campus}/>
                <div className="categories_page container-fluid vh-100">
                    <CategoriesContent/>
                </div>
            </>
        );
    }
}

export default CategoriesMainPage;