import React from "react";
import DetailsContent from "./DetailsContent";
import DescriptionRow from "../components/DescriptionRow";
import TopBar from "../components/TopBar";
import "./DetailsMainPage.css";

class DetailsMainPage extends React.Component {

    render(){
        return(
            <>
                <TopBar backText={"Categories"} backPath={"/cat"} campus={this.props.campus}/>
                <div className="details_page container-fluid vh-100">
                    <DetailsContent category={this.props.category}/>
                    <DescriptionRow title={this.props.category}/>
                </div>
            </>
        );
    }
}

export default DetailsMainPage;