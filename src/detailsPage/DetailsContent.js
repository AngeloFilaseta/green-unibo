import React from "react";
import InnerCategoryLabel from "../components/InnerCategoryLabel";
import {detailsItems} from "../helpers/Json";
import {newLineStr} from "../helpers/Strings";
import {makeIterator} from "../helpers/Helpers";
import RedirectImageTree from "../components/RedirectImageTree";
import voca from "voca";

class DetailsContent extends React.Component {

    n_elem = detailsItems(this.props.category).length;

    render() {
        const actualLink="/cat/" + voca.kebabCase(this.props.category) + "/";
        /** Get the items, and make an iterator out of the array, for Names and for Links */
        const labelList = detailsItems(this.props.category);

        const labelListNewLine = labelList.map((item) => newLineStr(item));
        const labelLinksList = labelList.map((item) => actualLink + voca.kebabCase(item));

        const itName = makeIterator(labelListNewLine);
        const itLink = makeIterator(labelLinksList);

        return (
            <div className="row h-xl-75 pt-5 align-items-end">
                <div id="tree-0" className="col-xl-3 pr-5 pl-6 tree_front clickable">
                    <RedirectImageTree treeNumber={3} link={itLink.next().value}/>
                    <InnerCategoryLabel additionalClass="tree_1_label text-right" label={itName.next().value}/>
                </div>
                <div  id="tree-1" className="col-xl-2 pl-6 pb-4 col-xl-pull-2 tree_front clickable">
                    <RedirectImageTree treeNumber={4} link={itLink.next().value}/>
                    <InnerCategoryLabel additionalClass="tree_2_label text-right text-lg-left" label={itName.next().value}/>
                </div>
                <div  id="tree-2" className="col-xl-2 pb-5 col-xl-pull-2 tree_front clickable">
                    <RedirectImageTree treeNumber={2} link={itLink.next().value}/>
                    <InnerCategoryLabel additionalClass="tree_3_label text-right" label={itName.next().value}/>
                </div>
                <div  id="tree-3" className="col-xl-2 pr-6 col-xl-pull-3 tree_front clickable">
                    <RedirectImageTree treeNumber={6} link={itLink.next().value}/>
                    <InnerCategoryLabel additionalClass="tree_4_label text-left text-lg-right" label={itName.next().value}/>
                </div>
                <div  id="tree-4" className="col-xl-3 px-5 pb-3 col-xl-pull-5 tree_middle clickable">
                    <RedirectImageTree treeNumber={7} link={itLink.next().value}/>
                    <InnerCategoryLabel additionalClass="tree_5_label text-left" label={itName.next().value}/>
                </div>
            </div>
        );
    }

    componentDidMount() {
        for (let i = 4; i >= this.n_elem; i--) {
            console.log(this.n_elem);
            let link = document.getElementById('tree-' + i);
            link.style.display = 'none';
        }
    }
}

export default DetailsContent;