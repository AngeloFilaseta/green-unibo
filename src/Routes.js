import React, { Component } from "react";
import {Router, Switch, Route, Redirect} from "react-router-dom";

import CampusMainPage from "./campusPage/CampusMainPage";
import CategoriesPage from "./categoriesPage/CategoriesMainPage";
import history from "./history";
import DetailsMainPage from "./detailsPage/DetailsMainPage";
import ArgumentMainPage from "./argumentPage/ArgumentMainPage";

export default class Routes extends Component {
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Redirect exact from="/" to="/cesena" />
                    {/* CAMPUS MAIN PAGE */}
                    <Route
                        path='/cesena'
                        render={(props) => <CampusMainPage {...props} campus={"cesena"} />}
                    />
                    <Route
                        path='/forli'
                        render={(props) => <CampusMainPage {...props} campus={"forlì"} />}
                    />
                    <Route
                        path='/bologna'
                        render={(props) => <CampusMainPage {...props} campus={"bologna"} />}
                    />
                    {/** ARGUMENTS */}

                    {/* Digital Communication*/}
                    <Route
                        path='/cat/comunicazione-digitale/almaorienta'
                        render={(props) => <ArgumentMainPage name="Almaorienta" campus={"cesena"}  category={"Comunicazione Digitale"}/>}
                    />
                    {/* Dematerialization */}
                    <Route
                        path='/cat/dematerializzazione/atti-organi-ateneo'
                        render={(props) => <ArgumentMainPage name="Atti Organi Ateneo" campus={"cesena"}  category={"Dematerializzazione"}/>}
                    />
                    <Route
                        path='/cat/dematerializzazione/contratti-di-docenza'
                        render={(props) => <ArgumentMainPage name="Contratti di Docenza" campus={"cesena"}  category={"Dematerializzazione"}/>}
                    />
                    <Route
                        path='/cat/dematerializzazione/tesi-digitali'
                        render={(props) => <ArgumentMainPage name="Tesi Digitali" campus={"cesena"}  category={"Dematerializzazione"}/>}
                    />
                    <Route
                        path='/cat/dematerializzazione/questionario-opinione-studenti'
                        render={(props) => <ArgumentMainPage name="Questionario Opinione Studenti" campus={"cesena"}  category={"Dematerializzazione"}/>}
                    />
                    <Route
                        path='/cat/dematerializzazione/contratti-per-rtd'
                        render={(props) => <ArgumentMainPage name="Contratti per RTD" campus={"cesena"}  category={"Dematerializzazione"}/>}
                    />
                    {/* Innovation */}
                    <Route
                        path='/cat/innovazione-del-processo/passaggio-nuova-firma-digitale'
                        render={(props) => <ArgumentMainPage name="Passaggio Nuova Firma Digitale" campus={"cesena"}  category={"Innovazione del Processo"}/>}
                    />
                    <Route
                        path='/cat/innovazione-del-processo/unatantum'
                        render={(props) => <ArgumentMainPage name="Unatantum" campus={"cesena"}  category={"Innovazione del Processo"}/>}
                    />

                    {/** CATEGORIES */}
                    <Route
                        path='/cat/comunicazione-digitale'
                        render={(props) => <DetailsMainPage campus={"cesena"}  category={"Comunicazione Digitale"}/>}
                    />
                    <Route
                        path='/cat/dematerializzazione'
                        render={(props) => <DetailsMainPage campus={"cesena"}  category={"Dematerializzazione"}/>}
                    />
                    <Route
                        path='/cat/innovazione-del-processo'
                        render={(props) => <DetailsMainPage campus={"cesena"}  category={"Innovazione del Processo"}/>}
                    />
                    <Route
                        path='/cat'
                        render={(props) => <CategoriesPage {...props} campus={"cesena"}/>}
                    />
                </Switch>
            </Router>
        )
    }
}