import React from "react";
import DescriptionRow from "../components/DescriptionRow";
import TopBar from "../components/TopBar";
import voca from "voca";
import ArgumentContent from "./ArgumentContent";

class ArgumentMainPage extends React.Component {

    render(){
        const backLink = "/cat/"+ voca.kebabCase(this.props.category);

        return(
            <>
                <TopBar backText={this.props.category} backPath={backLink} campus={this.props.campus}/>
                <div className="details_page container-fluid vh-100">
                    <ArgumentContent argument={this.props.name}/>
                    <DescriptionRow title={this.props.name}/>
                </div>
            </>
        );
    }

}

export default ArgumentMainPage;