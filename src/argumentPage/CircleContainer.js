import React from "react";
import Circle from "../components/Circle";
import ReactAnime from "react-animejs";
import {Colors} from "../helpers/Colors.ts"
const {Anime, stagger} = ReactAnime;

class CircleContainer extends React.Component {

    render(){
        const idContainer = "svg-container";
        /* Strings for circle description */
        let treeSavedLabel = "Alberi Salvati";
        let paperLabel = "Fogli di Carta";
        let co2AbsorvedLabel = "CO₂ Assorbita";
        let co2AvoidedLabel = "CO₂ Evitata";

        /* Example values */
        let treeSaved = 236;
        let paper = 1790000;
        let co2Absorved = 180;
        let co2Avoided = 1320;

        const DT_CIRCLE_APPEAR = 500;

        return(
            <Anime initial={[{
                targets: 'svg g',
                duration: 5000,
                scale:[0,1],
                delay: stagger(DT_CIRCLE_APPEAR) // increase delay by DT_CIRCLE_APPEAR ms for each elements.
            }]}>
                <svg viewBox={"0 300 1100 200"} height={"100%"} width={"100%"} id={idContainer}>
                    <Circle id="c1" x={210} y={300} containerId={idContainer} color={Colors.Green} radius={100}
                            title={treeSaved} text={treeSavedLabel} textColor={Colors.White}/>

                    <Circle id="c2" x={480} y={250} containerId={idContainer} color={Colors.Yellow} radius={150}
                            title={paper} text={paperLabel} textColor={Colors.White}/>

                    <Circle id="c3" x={720} y={360} containerId={idContainer} color={Colors.Blue} radius={85}
                            title={co2Absorved} text={co2AbsorvedLabel} textColor={Colors.Cyan}/>

                    <Circle id="c4" x={850} y={200} containerId={idContainer} color={Colors.Cyan} radius={100}
                            title={co2Avoided} text={co2AvoidedLabel} textColor={Colors.Blue}/>
                </svg>
            </Anime>

        );
    }


}

export default CircleContainer;
