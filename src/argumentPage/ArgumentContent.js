import React from "react";
import ImageTree from "../components/ImageTree";
import CircleContainer from "./CircleContainer";

class ArgumentContent extends React.Component {

    render(){

        return(
            <div className="row h-75 pt-7 align-items-end">
                <div className="col-lg-2 align-self-end ml-7 tree_middle d-none d-lg-block">
                    <ImageTree treeNumber={3}/>
                </div>
                <div className="col pb-5 pb-md-0">
                    <CircleContainer/>
                </div>
            </div>
        );
    }
}

export default ArgumentContent;
