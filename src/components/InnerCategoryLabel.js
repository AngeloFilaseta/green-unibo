import React from "react";
import ReactAnime from "react-animejs";
const {Anime} = ReactAnime;

class InnerCategoryLabel extends React.Component {

    render() {

        const titleNumber = this.props.title !== undefined ? parseInt(this.props.title, 10) : undefined;
        return (
            <Anime
                initial={[{
                    targets: ".title_label",
                    innerHTML: titleNumber,
                    easing: 'linear',
                    round: 1
                }]}>
                <div className={"position-absolute " + this.props.additionalClass}>
                    { titleNumber !== undefined &&
                    <h2 className="title_label">
                        0
                    </h2>
                    }
                    <p>
                        {this.props.label}
                    </p>
                </div>
            </Anime>
        );
    }
}

export default InnerCategoryLabel;
