import React from "react";

class MaterialIcon extends React.Component {
    render() {
        return (
            <i className={"mdi clickable " + this.props.additionalClasses + " " + this.props.icon}>{this.props.icon}</i>
        );
    }
}

export default MaterialIcon;