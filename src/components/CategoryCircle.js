import React from "react";
import Snap from "snapsvg-cjs"
import Circle from "./Circle";
import {Colors} from "../helpers/Colors.ts";

class CategoryCircle extends React.Component{

    componentDidMount() {
        const s = Snap("#cat" + this.props.catId);
        Snap.load("../images/tree_1.svg", (img) => {
            const el = img.select("g");
            s.append(el);
        });
    }

    render() {
        const circleText = "Alberi Salvati";
        const circleTitle = this.props.title;

        return(
            <>
                { this.props.catId === 1 &&
                <svg viewBox={"0 0 260 260"} height={"100%"} width={"100%"} id = {"c_"+ this.props.catId}>
                    <Circle id="c1" x={115} y={110} containerId={"c_" + this.props.catId}
                            color={Colors.Green} radius={100} title={circleTitle} text={circleText} textColor={Colors.White}/>);
                </svg>
                }

                { this.props.catId === 2 &&
                <svg viewBox={"0 0 340 340"} height={"100%"} width={"100%"} id = {"c_"+ this.props.catId}>
                    <Circle id="c2" x={195} y={125} containerId={"c_" + this.props.catId}
                            color={Colors.Blue} radius={100} title={circleTitle} text={circleText} textColor={Colors.Cyan}/>);
                </svg>
                }
                { this.props.catId === 3 &&
                <svg viewBox={"0 0 260 260"} height={"100%"} width={"100%"} id = {"c_"+ this.props.catId}>
                    <Circle id="c3" x={115} y={120} containerId={"c_" + this.props.catId}
                            color={Colors.Yellow} radius={100} title={circleTitle} text={circleText} textColor={Colors.White}/>);
                </svg>
                }
            </>
        );
    }
}




export default CategoryCircle;