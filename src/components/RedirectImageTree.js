import ImageTree from "./ImageTree";
import React from "react";
import history from "../history";

class RedirectImageTree extends React.Component {
    render() {
        return (
            <div onClick={() => history.push(this.props.link)}>
                <ImageTree treeNumber={this.props.treeNumber}/>
            </div>
        );
    }
}

export default RedirectImageTree;
