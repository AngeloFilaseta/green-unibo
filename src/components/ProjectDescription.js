import React from "react";

class ProjectDescription extends React.Component {

    render() {
        return (
            <p>
                {this.props.description}
            </p>
        );
    }

}

export default ProjectDescription;
