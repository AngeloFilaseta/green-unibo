import React from "react";
import voca from "voca";
import ImageTree from "./ImageTree";
import Snap from "snapsvg-cjs"
import InnerCategoryLabel from "./InnerCategoryLabel";
import IconAndProject from "./IconAndProject";
import CategoryCircle from "./CategoryCircle";
import ReactAnime from "react-animejs";
const {Anime} = ReactAnime;


class Category extends React.Component{

    componentDidMount() {
        const s = Snap("#cat" + this.props.catId);
        Snap.load("../images/tree_1.svg", (img) => {
            const el = img.select("g");
            s.append(el);
        });
    }

    render() {

        let link = "/cat/" + voca.kebabCase(this.props.name);
        let catIdClass = "cat" + this.props.catId;
        let textLabelCo2 = "Kg di CO2";

        /* Example Values */
        let saveTreeValue = 123;
        let labelCo2Value = 236;

        return(
            <>
                <div className="row">
                    <div id={catIdClass} className={"col h-75 clickable " + this.props.treeSpacing}>
                        <ImageTree treeNumber={this.props.treeNumber}/>
                    </div>
                </div>
                    {this.props.selected === true &&
                    <Anime initial={[{
                        targets: [".label_category_co2", "g"],
                        scale: [0, 1],
                        duration: 1000,
                    }]}>
                    <div className="position-absolute circle row">
                        <div className={"col h-75 " + this.props.treeSpacing}>
                            <CategoryCircle catId={this.props.catId} title ={saveTreeValue}/>
                        </div>
                        <InnerCategoryLabel additionalClass="label_category_co2 text-center"
                                            title={labelCo2Value} label={textLabelCo2}/>
                    </div>
                    </Anime>
                    }
                <IconAndProject name={this.props.name} icon={this.props.icon} link={link}/>
            </>
        );
    }
}




export default Category;