import React from 'react';
import {Progress} from 'reactstrap';
import ProgressItemList from './ProgressItemList'
import {linspace} from "../helpers/Helpers";

function TextRow(props){
    return(
        <div className="row mb-2">
            <div className="col">
                <ProgressItemList list = {props.list} textRotation = "rotate-l-90" selected = {props.selected}/>
            </div>
        </div>
    );
}

function ProgressRow(props){
    return(
        <div className="row py-3">
            <div className="col">
                <Progress value={calculateProgressValue(props.list, props.selected)}/>
            </div>
        </div>
    );
}

function calculateProgressValue(list, selected){
    if(selected !=null){
        const values = linspace(0,100, list.length);
        values[0]+=5;
        return values[list.indexOf(selected)];
    }
    return 100;
}

class ProgressWithItems extends React.Component{

    render() {
        const verticalClass = this.props.vertical === true ? "rotate-r-90" : "";
        const textBot = this.props.textBot === true;

        if(textBot){
            return(
                <div className={verticalClass}>
                    <ProgressRow list={this.props.list} selected={this.props.selected}/>
                    <TextRow list={this.props.list} selected={this.props.selected}/>
                </div>
            );
        }else{
            return(
                <div className={verticalClass}>
                    <TextRow list={this.props.list} selected={this.props.selected} />
                    <ProgressRow list={this.props.list} selected={this.props.selected}/>
                </div>
            );
        }

    }
}

export default ProgressWithItems;