import React from 'react';
import Snap from 'snapsvg-cjs';
import {deltaSpaceFromRadius, textFontSizeFromRadius, titleFontSizeFromRadius} from "../helpers/Svg";
import {dotNotationNumber} from "../helpers/Strings";

class Circle extends React.Component {

    render(){
        return null;
    }

    componentDidMount() {

        const DT_NUMBER_LOADING = 2500;

        let xPos = this.props.x;
        let yPos = this.props.y;
        let radius = this.props.radius != null ? this.props.radius : 100;
        let title = this.props.title.toString();
        let fontSizeTitle = titleFontSizeFromRadius(radius, title.length);
        let fontSizeText = textFontSizeFromRadius(radius, this.props.text.length);
        let deltaSpace = deltaSpaceFromRadius(radius);

        const s = Snap("#" + this.props.containerId.toString());

        const circle = s.circle(xPos, yPos, radius);

        circle.attr({
            fill: this.props.color
        });

        const titleLabel = s.text(xPos,yPos);
        titleLabel.attr({
            fontSize: fontSizeTitle,
            fontFamily:"Open Sans Bold",
            fontWeight:"bold",
            fill: this.props.textColor,
            textAnchor:"middle"
        });
        const textLabel = s.text(xPos,yPos + deltaSpace, this.props.text);
        textLabel.attr({
            fontSize: fontSizeText,
            fontFamily:"Open Sans",
            fill: this.props.textColor,
            textAnchor:"middle"
        });

        const fullCircle = s.group(circle, titleLabel, textLabel);
        fullCircle.attr({
            id: this.props.id
        });

        Snap.animate(0, title, function (value) {
            titleLabel.attr({
                fontSize: fontSizeTitle,
                text: dotNotationNumber(Math.round(value))});
        }, DT_NUMBER_LOADING);
    }


}

export default Circle;