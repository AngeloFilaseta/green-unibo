import React from "react";
import ProgressWithItems from "./ProgressWithItems";
import {getYearsList} from "../helpers/Helpers";
import TitleLabel from "./TitleLabel";
import ProjectDescription from "./ProjectDescription";
import {description} from "../helpers/Json";

class DescriptionRow extends React.Component {

    render() {
        let yearsValues = getYearsList();

        return (
            <div className="row h-25 ">
                <div className="col-xl-4 align-self-end pl-4  pt-md-5">
                    <ProgressWithItems list={yearsValues}/>
                </div>
                <div className="col-xl-4 col-12 align-self-end ">
                    <TitleLabel title={this.props.title}/>
                </div>
                <div className="col-xl-4 align-self-end ">
                    <ProjectDescription  description={description(this.props.title)} />
                </div>
            </div>
        );

    }
}

export default DescriptionRow;