import React from "react";
import MaterialIcon from "./MaterialIcon";
import history from "../history";
import ReactAnime from "react-animejs";
import {Colors} from "../helpers/Colors.ts"
const {Anime} = ReactAnime;

class IconAndProject extends React.Component{

    render() {

        return(
                <Anime
                    _onMouseEnter={[{
                        targets: "." + this.props.icon,
                        scale: 1.2,
                        color: Colors.Blue
                    }]}
                    _onMouseLeave={[{
                        targets: "." + this.props.icon,
                        scale: 1.0,
                        color: Colors.Black
                    }]}>
                    <div className="row pt-5">
                        <div onClick={() => history.push(this.props.link)} className="col text-center">
                            <MaterialIcon additionalClasses={"mdi-4x"} icon={this.props.icon}/>
                            <br/>
                            <h5 className={this.props.icon}>{this.props.name}</h5>
                        </div>
                    </div>
                </Anime>
        );
    }
}




export default IconAndProject;