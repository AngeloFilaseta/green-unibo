import React from "react";
import MaterialIcon from "./MaterialIcon";
import {Colors} from "../helpers/Colors.ts";

class ClickMeLabel extends React.Component {
    render() {
        let text = this.props.text === undefined ? "" : this.props.text.toString();
        let color = this.props.color === undefined ? Colors.Black : this.props.color.toString();
        let icon = "touch_app";

        return (
            <div style={{color: color}} className={"click_me text-center"}>
                <h3>{text}</h3>
                <MaterialIcon icon ={icon} additionalClasses={" text-center mdi-3x"}/>
            </div>
        );
    }
}

export default ClickMeLabel;