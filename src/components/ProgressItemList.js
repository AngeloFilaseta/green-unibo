import React from 'react';
import voca from "voca";

class ProgressItemList extends React.Component {

    render() {
        const textRotProperty = this.props.textRotation != null ? this.props.textRotation.toString()  : "";
        const selected = this.props.selected != null ? voca.capitalize(this.props.selected.toString()) : "";
        const listItem = this.props.list;
        const row_cols_n  = "row-cols-" + listItem.length;

        let list = this.props.list.map( function(item, index){
            if(selected === item){
                return <div key={index} className={"col text-center selected_item " + textRotProperty}>{item}</div>
            }else{
                return <div key={index} className={"col text-center " + textRotProperty }>{item}</div>
            }
        });

        return (
            <div className={"row justify-content-center" + row_cols_n }>
                {list}
            </div>
        );

    }
}

export default ProgressItemList;
