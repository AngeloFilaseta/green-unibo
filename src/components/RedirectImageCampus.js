import React from "react";
import history from "../history";
import ImageCampus from "./ImageCampus";

class RedirectImageCampus extends React.Component {
    render() {
        const link = this.props.link !== undefined ? this.props.link : "/" + this.props.campus.toString();

        return (
            <div id="campus" onClick={() => history.push(link)}>
                <ImageCampus name={this.props.campus}/>
            </div>
        );
    }
}

export default RedirectImageCampus;
