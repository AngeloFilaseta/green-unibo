import BackArrow from "./BackArrow";
import React from "react";
import {campusFullNameNewLine} from "../helpers/Strings";
import RedirectImageCampus from "./RedirectImageCampus";
import ReactAnime from "react-animejs";
const {Anime} = ReactAnime;

class TopBar extends React.Component {
    render() {

        const campusName = campusFullNameNewLine(this.props.campus);

        return (
            <div className="topbar position-absolute w-100">
                <div className="navbar navbar-default navbar-static-top">
                    <div className="container-fluid">
                        <div className="row w-100">
                            <div className="col-2 pb-5 pr-3">
                                <BackArrow text={this.props.backText} path={this.props.backPath}/>
                            </div>
                            <div className="col-md-6 col-5">
                                {/* Nothing */}
                            </div>
                            <div className="position-relative col-3 col-md-2">
                                <Anime
                                    _onMouseEnter={[
                                        {
                                            targets: ".campus_topbar",
                                            scale: 1.1
                                        }
                                    ]}
                                    _onMouseLeave={[
                                        {
                                            targets: ".campus_topbar",
                                            scale: 1.0
                                        }
                                    ]}
                                >
                                    <div className="campus_topbar position-absolute pt-lg-3 pt-4 pt-4">
                                        <RedirectImageCampus campus={this.props.campus}/>
                                    </div>
                                </Anime>
                            </div>
                            <div className="col-2 pt-3 text-center">
                                <p>{campusName}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        );
    }
}

export default TopBar;
