import React from 'react';

import Tree1 from '../images/tree_1.svg';
import Tree2 from '../images/tree_2.svg';
import Tree3 from '../images/tree_3.svg';
import Tree4 from '../images/tree_4.svg';
import Tree5 from '../images/tree_5.svg';
import Tree6 from '../images/tree_6.svg';
import Tree7 from '../images/tree_7.svg';
import Tree8 from '../images/tree_8.svg';
import Tree9 from '../images/tree_9.svg';

class ImageTree extends React.Component {

    render() {
        switch (this.props.treeNumber) {
            case 1:
                return <img src={Tree1} className="img-fluid" alt="Alberello"/>
            case 2:
                return <img src={Tree2} className="img-fluid" alt="Alberello"/>
            case 3:
                return <img src={Tree3} className="img-fluid" alt="Alberello"/>
            case 4:
                return <img src={Tree4} className="img-fluid" alt="Alberello"/>
            case 5:
                return <img src={Tree5} className="img-fluid" alt="Alberello"/>
            case 6:
                return <img src={Tree6} className="img-fluid" alt="Alberello"/>
            case 7:
                return <img src={Tree7} className="img-fluid" alt="Alberelli"/>
            case 8:
                return <img src={Tree8} className="img-fluid" alt="Boschetto"/>
            case 9:
                return <img src={Tree9} className="img-fluid" alt="Boschetto"/>
            case undefined:
                return <img src={Tree1} className="img-fluid" alt="Alberello"/>
            default:
                return <img src={Tree1} className="img-fluid" alt="Alberello"/>
        }
    }
}

export default ImageTree;
