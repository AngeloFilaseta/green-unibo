import React from 'react';
import history from "../history";
import MaterialIcon from "./MaterialIcon";


class BackArrow extends React.Component {
    render() {
        return (
            <button className="btn" onClick={() => history.push(this.props.path)}>
                <div className="media">
                    <MaterialIcon additionalClasses={"back_arrow mdi-2x align-self-center mr-3"} icon={"arrow_back"}/>
                    <div className="media-body pt-3">
                        <p>{this.props.text}</p>
                    </div>
                </div>
            </button>
        );
    }
}

export default BackArrow;