import React from 'react';

class TitleLabel extends React.Component {
    render() {
        return (
            <div className="text-xl-right">
                <h1>{this.props.title}</h1>
            </div>
        );
    }
}

export default TitleLabel;