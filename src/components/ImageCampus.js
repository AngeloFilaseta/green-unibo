import React from 'react';
import CesenaCampus from '../images/cesena.svg'
import NavileCampus from '../images/bologna.svg'
import ForliCampus from '../images/forli.svg'

class ImageCampus extends React.Component {

    render() {
        const campusName = this.props.name != null ? this.props.name.toString() : "";

        switch (campusName) {
            case "cesena":
                return <img src={CesenaCampus} className="img-fluid" alt="Cesena Campus"/>
            case "navile":
                return <img src={NavileCampus} className="img-fluid" alt="Navile Campus"/>
            case "forlì":
                return <img src={ForliCampus} className="img-fluid" alt="Forlì Campus"/>
            default:
                console.log("Campus Images not found. The campus name is:" + campusName);
                return <h1 className="text-danger text-center">Campus {campusName} is missing, was it demolished?</h1>
        }
    }
}

export default ImageCampus;
