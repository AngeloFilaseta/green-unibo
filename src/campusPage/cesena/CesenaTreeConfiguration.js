import React from "react";
import ImageTree from "../../components/ImageTree";

class CampusConfiguration extends React.Component{

    render() {
        return (
            <>
                <div className="col-6">
                    {/* Nothing */}
                </div>
                <div className="tree_front pt-7 col-1 col-md-push-2 col-push-2 d-none d-md-block">
                    {/* Small Tree, white leaf*/}
                    <ImageTree treeNumber={1} />
                </div>
                <div className="tree_back col-2 col-md-push-2 col-push-3 d-none d-md-block">
                    {/* Medium Tree, white leaf*/}
                    <ImageTree treeNumber={2} />
                </div>
                <div className="tree_front col-3 col-md-2 col-md-push-1 col-push-2">
                    {/* Bigger Tree, Blue leaf*/}
                    <ImageTree treeNumber={7} />
                </div>
            </>
        );
    }
}

export default CampusConfiguration;
