import React from "react";
import RedirectImageCampus from "../../components/RedirectImageCampus";
import ClickMeLabel from "../../components/ClickMeLabel";
import {Colors} from "../../helpers/Colors.ts";

class CesenaCampusContainer extends React.Component{

    render(){

        let clickMeText = "Prova a cliccarmi!";
        let campus = "cesena";
        let backLink = "/cat";

        return(
            <div className="position-absolute campus row align-items-center">
                <div className={"col-8"}>
                    <ClickMeLabel text={clickMeText} color={Colors.Blue}/>
                    <RedirectImageCampus campus={campus} link={backLink}/>
                </div>
            </div>
        );
    }
}

export default CesenaCampusContainer;