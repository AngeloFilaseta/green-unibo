import React from "react";
import CesenaCampusContainer from "./cesena/CesenaCampusContainer";
import ReactAnime from "react-animejs";
const {Anime} = ReactAnime;

class CampusContainer extends React.Component{

    render() {
        return (
            <Anime initial={[{
                targets: '.click_me',
                keyframes: [
                    {
                        scale: [2, 1],
                        opacity: [0, 1],
                        duration: 4000,
                        delay: 5000
                    },
                    {
                        duration: 3000,
                        opacity: [1, 0],
                        scale: 0
                    }],
                loop: true
            }]}>
                {this.props.campus==="cesena" && <CesenaCampusContainer/>}
            </Anime>
        );
    }
}

export default CampusContainer;

