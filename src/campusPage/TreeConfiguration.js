import React from "react";
import CesenaTreeConfiguration from "./cesena/CesenaTreeConfiguration";

class TreeConfiguration extends React.Component{

    render() {
        return (
                <>
                    {this.props.campus==="cesena" && <CesenaTreeConfiguration/>}
                </>
        );
    }
}

export default TreeConfiguration;

