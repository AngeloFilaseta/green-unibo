import React from 'react';
import Footer from "./Footer";
import GreenHill from '../images/green_hill.svg';
import "./CampusPage.css";
import CampusContainer from "./CampusContainer";
import TreeConfiguration from "./TreeConfiguration";

class CampusMainPage extends React.Component{

    render() {

        let altGreenHill = "Collinetta Verde";

        return (
            <>
                <div className="campus_page container-fluid vh-100 bg-gradient-green">
                    <CampusContainer campus = {this.props.campus}/>
                    <div className="row h-100 align-items-center pb-7">
                        <TreeConfiguration campus = {this.props.campus}/>
                    </div>
                </div>
                <img src={GreenHill} className="hill position-absolute" alt={altGreenHill}/>
                <Footer campus={this.props.campus}/>
            </>
        );
    }
}

export default CampusMainPage;
