import React from 'react';

import TitleLabel from '../components/TitleLabel.js';
import ProgressWithItems from '../components/ProgressWithItems.js';
import ProjectDescription from '../components/ProjectDescription.js';

import voca from "voca";
import {getCampusList} from "../helpers/Helpers";
import {campusFullName} from "../helpers/Strings";


class Footer extends React.Component{

    render() {
        let campusName = campusFullName(this.props.campus);

        let footerText = "Dal 2016, l’Università di Bologna ha potenziato la digitalizzazione" +
        "dei processi amministrativi e di comunicazione per incrementare il " +
        "risparmio della carta, favorire il benessere ambientale e salvaguardare " +
        "gli alberi. Il progetto ReMade nasce così per tradurre in modo proporzionale " +
        "il risparmio di carta e piantumazione di nuovi alberi.";

        return (
            <div className="campus_page footer position-absolute w-100">
                <div className="navbar navbar-default navbar-static-bottom">
                    <div className="container-fluid pt-5">
                        <div className="row">
                            <div className="col-xl-5 align-self-center d-none d-xl-block">
                                <ProgressWithItems list= {getCampusList()}
                                                   selected={voca.capitalize(this.props.campus)}/>
                            </div>
                            <div className="col col-md-3 col-lg-3 col-xl-2 my-4 align-self-end">
                                <TitleLabel title={campusName}/>
                            </div>
                            <div className="col col-md-9 col-lg-9 col-xl-5 align-self-end d-none d-md-block ">
                                <ProjectDescription description={footerText}/>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Footer;